CREATE TABLE node_tree
(
	idNode INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	level INT UNSIGNED NOT NULL,
	iLeft INT UNSIGNED NOT NULL,
	iRight INT UNSIGNED NOT NULL
);

CREATE TABLE node_tree_names
(
	idNode INT UNSIGNED NOT NULL,
	language ENUM('english','italian') NOT NULL,
	nodeName VARCHAR(255),
	PRIMARY KEY (idNode, language),
	FOREIGN KEY (idNode)
        REFERENCES node_tree (idNode)
        ON UPDATE CASCADE ON DELETE CASCADE
)
