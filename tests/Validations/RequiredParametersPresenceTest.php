<?php

use App\App;
use App\Validations\RequiredParametersPresence;
use App\Exceptions\RequiredParametersPresenceException;

class RequiredParametersPresenceTest extends \PHPUnit_Framework_TestCase
{

    protected $requiredParametersPresence;

    protected function setUp()
    {
        App::init();
        $this->requiredParametersPresence = new RequiredParametersPresence();
    }

    protected function tearDown()
    {
        $this->requiredParametersPresence = null;
    }

    public function validateThrowsExceptionProvider()
    {
        return [
            [
                []
            ],
            [
                ["node_id" => 1]
            ],
            [
                ["language" => "italian"]
            ]
        ];
    }

    /**
     * @dataProvider validateThrowsExceptionProvider
     */
    public function testValidateThrowsException($array)
    {
        $this->expectException(RequiredParametersPresenceException::class);
        $this->requiredParametersPresence->validate($array);
    }

    public function testValidateWithoutException()
    {
        $this->requiredParametersPresence->validate(
            [
                "node_id" => 1,
                "language" => "italian"
            ]
        );
        $this->addToAssertionCount(1);
    }

}
