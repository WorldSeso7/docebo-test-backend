<?php

use App\App;
use App\Validations\PageSizeInRange;
use App\Exceptions\PageSizeInRangeException;

class PageSizeInRangeTest extends \PHPUnit_Framework_TestCase
{

    protected $pageSizeInRange;

    protected function setUp()
    {
        App::init();
        $this->pageSizeInRange = new PageSizeInRange();
    }

    protected function tearDown()
    {
        $this->pageSizeInRange = null;
    }

    public function validateThrowsExceptionProvider()
    {
        return [
            [
                ["page_size" => -5]
            ],
            [
                ["page_size" => 0]
            ],
            [
                ["page_size" => 1000]
            ],
            [
                ["page_size" => 9999]
            ],
            [
                ["page_size" => "string_value"]
            ]
        ];
    }

    /**
     * @dataProvider validateThrowsExceptionProvider
     */
    public function testValidateThrowsException($array)
    {
        $this->expectException(PageSizeInRangeException::class);
        $this->pageSizeInRange->validate($array);
    }


    public function validateNotThrowsExceptionProvider()
    {
        return [
            [
                []
            ],
            [
                ["page_size" => 1]
            ],
            [
                ["page_size" => 123]
            ],
            [
                ["page_size" => 999]
            ]
        ];
    }

    /**
     * @dataProvider validateNotThrowsExceptionProvider
     */
    public function testValidateWithoutException($array)
    {
        $this->pageSizeInRange->validate($array);
        $this->addToAssertionCount(1);
    }

}
