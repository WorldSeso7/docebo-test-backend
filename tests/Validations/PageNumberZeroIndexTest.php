<?php

use App\App;
use App\Validations\PageNumberZeroIndex;
use App\Exceptions\PageNumberZeroIndexException;

class PageNumberZeroIndexTest extends \PHPUnit_Framework_TestCase
{

    protected $pageNumberZeroIndex;

    protected function setUp()
    {
        App::init();
        $this->pageNumberZeroIndex = new PageNumberZeroIndex();
    }

    protected function tearDown()
    {
        $this->pageNumberZeroIndex = null;
    }

    public function validateThrowsExceptionProvider()
    {
        return [
            [
                ["page_num" => -5]
            ],
            [
                ["page_num" => "string_value"]
            ]
        ];
    }

    /**
     * @dataProvider validateThrowsExceptionProvider
     */
    public function testValidateThrowsException($array)
    {
        $this->expectException(PageNumberZeroIndexException::class);
        $this->pageNumberZeroIndex->validate($array);
    }


    public function validateNotThrowsExceptionProvider()
    {
        return [
            [
                []
            ],
            [
                ["page_num" => 0]
            ],
            [
                ["page_num" => 1]
            ],
            [
                ["page_num" => 999]
            ]
        ];
    }

    /**
     * @dataProvider validateNotThrowsExceptionProvider
     */
    public function testValidateWithoutException($array)
    {
        $this->pageNumberZeroIndex->validate($array);
        $this->addToAssertionCount(1);
    }

}
