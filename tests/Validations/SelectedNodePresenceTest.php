<?php

use App\App;
use App\Validations\SelectedNodePresence;
use App\Exceptions\SelectedNodePresenceException;

class SelectedNodePresenceTest extends \PHPUnit_Framework_TestCase
{
    protected $nodeIdPresence;

    protected function setUp()
    {
        App::init();
        $this->nodeIdPresence = new SelectedNodePresence();
    }

    protected function tearDown()
    {
        $this->nodeIdPresence = null;
    }

    public function testValidateThrowsException()
    {
        $this->expectException(SelectedNodePresenceException::class);
        $this->nodeIdPresence->validate(["node_id" => 30]);
    }

    public function testValidateWithoutException()
    {
        $this->nodeIdPresence->validate(["node_id" => "5"]);
        $this->addToAssertionCount(1);
    }

}
