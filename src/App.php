<?php

namespace App;

use App\DbConnections\MySqliConnection;
use App\Exceptions\RequiredParametersPresenceException;
use App\Exceptions\SelectedNodePresenceException;
use App\Exceptions\PageNumberZeroIndexException;
use App\Exceptions\PageSizeInRangeException;
use App\Validations\RequiredParametersPresence;
use App\Validations\SelectedNodePresence;
use App\Validations\PageNumberZeroIndex;
use App\Validations\PageSizeInRange;
use \mysqli;
use \Config;

class App
{
    public static $mysqli = null;

    protected $requestValidations = [
        RequiredParametersPresence::class,
        SelectedNodePresence::class,
        PageNumberZeroIndex::class,
        PageSizeInRange::class,
    ];

    public function __construct()
    {
        static::init();
    }

    public static function init()
    {
        static::$mysqli = new mysqli(Config::$dbHost, Config::$dbUsername, Config::$dbPassword, Config::$dbName, Config::$dbPort);
    }

    public function run()
    {
        $this->_validateRequestParameters();
        $this->_handleRequest();
    }

    private function _validateRequestParameters()
    {
        // validating $_GET parameters using specific classes
        try {
            foreach ($this->requestValidations as $validation) {
                (new $validation)->validate($_GET);
            }
        }
        catch (RequiredParametersPresenceException $requiredParametersPresenceException) {
            Response::error($requiredParametersPresenceException->getMessage());
        }
        catch (SelectedNodePresenceException $selectedNodePresenceException) {
            Response::error($selectedNodePresenceException->getMessage());
        }
        catch (PageNumberZeroIndexException $pageNumberZeroIndexException) {
            Response::error($pageNumberZeroIndexException->getMessage());
        }
        catch (PageSizeInRangeException $pageSizeInRangeException) {
            Response::error($pageSizeInRangeException->getMessage());
        }
    }

    private function _handleRequest()
    {
        // getting the request parameters
        $nodeId = $_GET["node_id"];
        $language = $_GET["language"];
        $searchKeyword = isset($_GET["search_keyword"]) ? mb_strtolower($_GET["search_keyword"]) : null;
        $pageNum = isset($_GET["page_num"]) ? intval($_GET["page_num"]) : 0;
        $pageSize = isset($_GET["page_size"]) ? intval($_GET["page_size"]) : 100;

        // preparing the query statement and parameters
        $query = App::$mysqli->prepare("
            SELECT
                found_nt.idNode AS node_id,
                ntn.nodeName AS name,
                COUNT(found_nt.idNode) AS children_count
            FROM
                node_tree AS selected_nt
                JOIN
                node_tree AS found_nt
                ON
                    selected_nt.iLeft < found_nt.iLeft
                    AND
                    selected_nt.iRight > found_nt.iRight
                    AND
                    selected_nt.level + 1 = found_nt.level
                JOIN
                    node_tree_names ntn
                ON
                    found_nt.idNode = ntn.idNode
                LEFT JOIN
                node_tree found_children_nt
                ON
                    found_nt.iLeft < found_children_nt.iLeft
                    AND
                    found_nt.iRight > found_children_nt.iRight
                    AND
                    found_nt.level + 1 = found_children_nt.level
            WHERE
                selected_nt.idNode = ?
                AND
                ntn.language = ?
                AND
                LOWER(ntn.nodeName) LIKE ?
            GROUP BY
                found_nt.idNode, ntn.nodeName
            LIMIT
                ?, ?
        ");
        $likeSearchKeyword = "%" . $searchKeyword . "%";
        $offset = $pageNum * $pageSize;
        $query->bind_param("issii", $nodeId, $language, $likeSearchKeyword, $offset, $pageSize);

        $nodes = [];
        if ($query->execute()) {
            $queryResult = $query->get_result();
            if ($queryResult->num_rows) {
                while($row = $queryResult->fetch_assoc()) {
                    // needed to correctly parse special characters when convertin in json format
                    $row = array_map("utf8_encode", $row);
                    $row["node_id"] = intval($row["node_id"]);
                    $row["children_count"] = intval($row["children_count"]);
                    $nodes[] = $row;
                }
            }
        }

        // returning nodes response
        Response::nodes($nodes);
    }
}
