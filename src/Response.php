<?php

namespace App;

class Response
{
    public static function nodes($nodes)
    {
        static::_sendResponse(["nodes" => $nodes]);
    }

    public static function error($message)
    {
        static::_sendResponse(["error" => $message, "nodes" => []]);
    }

    private static function _sendResponse($array)
    {
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($array);
        exit;
    }
}
