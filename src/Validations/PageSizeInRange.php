<?php

namespace App\Validations;

use App\Exceptions\PageSizeInRangeException;

class PageSizeInRange implements Validation
{
    public function validate($sourceArray)
    {
        // page_size is optional, so it needs to be validated only if present
        if (
            isset($sourceArray["page_size"])
            &&
            (
                intval($sourceArray["page_size"]) <= 0
                ||
                intval($sourceArray["page_size"]) >= 1000
                ||
                !ctype_digit(
                    strval(
                        $sourceArray["page_size"]
                    )
                )
            )
        ) {
            throw new PageSizeInRangeException("Invalid page size requested");
        }
    }
}
