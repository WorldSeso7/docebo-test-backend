<?php

namespace App\Validations;

use App\Exceptions\SelectedNodePresenceException;
use \App\App;

class SelectedNodePresence implements Validation
{
    public function validate($sourceArray)
    {
        // the specified node_id needs to exists in the db
        if (isset($sourceArray["node_id"])) {
            $nodeId = $sourceArray["node_id"];
            $query = App::$mysqli->prepare("
                SELECT * FROM node_tree WHERE idNode = ?
            ");
            $query->bind_param("i", $nodeId);
            if ($query->execute()) {
                $queryResult = $query->get_result();
                if (!$queryResult->num_rows) {
                    throw new SelectedNodePresenceException("Invalid node id");
                }
            }
        }
    }
}
