<?php

namespace App\Validations;

interface Validation
{
    public function validate($sourceArray);
}
