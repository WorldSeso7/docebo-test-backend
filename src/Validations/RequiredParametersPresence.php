<?php

namespace App\Validations;

use App\Exceptions\RequiredParametersPresenceException;

class RequiredParametersPresence implements Validation
{
    public function validate($sourceArray)
    {
        // node_id and language are required parameters
        if (
            empty($sourceArray["node_id"])
            ||
            empty($sourceArray["language"])
        ) {
            throw new RequiredParametersPresenceException("Missing mandatory params");
        }
    }
}
