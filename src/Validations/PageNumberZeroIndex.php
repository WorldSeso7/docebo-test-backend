<?php

namespace App\Validations;

use App\Exceptions\PageNumberZeroIndexException;

class PageNumberZeroIndex implements Validation
{
    public function validate($sourceArray)
    {
        // page_num is optional, so it needs to be validated only if present
        if (
            isset($sourceArray["page_num"])
            &&
            (
                intval($sourceArray["page_num"]) < 0
                ||
                !ctype_digit(
                    strval(
                        $sourceArray["page_num"]
                    )
                )
            )
        ) {
            throw new PageNumberZeroIndexException("Invalid page number requested");
        }
    }
}
