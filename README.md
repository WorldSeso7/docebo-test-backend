### Instructions

- Edit MySQL database connection parameters inside config.php file
- Run table.sql on your target MySQL database
- Run data.sql on your target MySQL database
- Test it!