<?php

class Config
{
	// MySQL database host
	public static $dbHost = "localhost";
	// MySQL connection user
	public static $dbUsername = "username";
	// MySQL connection password
	public static $dbPassword = "password";
	// MySQL database name
	public static $dbName = "name";
	// MySQL database port
	public static $dbPort = 3306;
}
